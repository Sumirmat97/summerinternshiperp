<!DOCTYPE html>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>IIITK | ERP</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- DataTables -->
<link rel="stylesheet"
	href="plugins/datatables/dataTables.bootstrap.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->





<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body >


		
		<%@ page import="java.util.ArrayList"%>
		<%@ page import="java.util.Iterator"%>
		<%@ page import="users.Student"%>



		<!-- Content Wrapper. Contains page content -->
		<div >
			<!-- Content Header (Page header) -->
			<section class="content-header">
		<center>	<h1>Indian Institute Of Information Technology, Kota Reporting</h1></center>
				
				
			</section>


			<!-- Main content -->
			<section class="content">
<div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>120</h3>

              <p style="font-size:150%">Seats</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
           
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><%=Query.getNewRegisteredStudentsCount()%></h3>

              <p style="font-size:150%">Registered Students</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><%=Query.getNewReportedStudentsCount()%></h3>

              <p style="font-size:150%">Students Reported</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><%=(120-Query.getNewReportedStudentsCount())%></h3>

              <p style="font-size:150%">Students Pending to report</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
				<div class="row">
					<div class="col-xs-12">

						<div class="box">
							
							<!-- /.box-header -->
							<div class="box-body" style="overflow-x: scroll;">
								<table id="example1" class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Registration Id</th>
											<th>Name</th>
											<th>Registration Status</th>
											<th>Student ID</th>
										</tr>
									</thead>
									<tbody>
										<%
											ArrayList<Student> registration_list = Query.getStudentRegistrationList();
											Iterator<Student> iterator = registration_list.iterator();
											while (iterator.hasNext()) {
												Student current = iterator.next();
												if(current.getSemester()!=1)continue;
												int status = current.getVerification_status();
												String status_msg = "";
												switch (status) {
													case 0 :
														status_msg = "reported";
														break;
													case 1 :
														status_msg = "data entered";
														break;
													case 2 :
														status_msg = "data approved";
														break;
													case 3 :
														status_msg = "fee paid";
														break;
													case 4 :
														status_msg = "fee verified";
														break;
													case 5 :
														status_msg = "registered";
														break;
													case 6 :
														status_msg = "Pass Out";
														break;
													case 7 :
														status_msg = "Course withdraw";
														break;
													case 8 :
														status_msg = "Terminate";
														break;
												}

												//0 reported
												//1 data entered
												// 2 approved
												// 3 fee paid
												// 4 fee approved
												//5 registered
										%>
										<tr>
											<td><%=current.getRegistration_id()%></td>
											<td><%=current.getName()%></td>
											<td><%=status_msg%></td>
											<td><%if (current.getStudent_id()!=null)out.print(current.getStudent_id());%></td>
										</tr>
										<%
											}
										%>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
				<!-- Small boxes (Stat box) -->
      
				
			</section>
			<!-- /.content -->
	
	
	
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- SlimScroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.min.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	
	<!-- page script -->
	<script src="dist/js/payment.js"></script>
	<script>
	var table;
		$(function() {
		 table=	$("#example1").DataTable({
				"paging" : true,
				"lengthChange" : false,
				"searching" : false,
				"ordering" : true,
					"autoWidth" : true
			});
		 var page=0;
		setInterval(function() {
			
			if( table.page.info().pages-1==page){
				window.location.reload();
				page=0;
			}
			 else {
				 page=page+1;
				 
			 }
			table.page( page ).draw(false);
			}, 5000);
		 
		});
		
	
	</script>
</body>
</html>