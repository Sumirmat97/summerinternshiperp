package actions.registrationFeePayment;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.feePayment.Query;
import postgreSQLDatabase.feePayment.Transaction;

/**
 * Servlet implementation class AddPaymentInfo
 */

@WebServlet(name = "Add Office Payment Info Servlet", urlPatterns = { "/AddOfficeRegistrationPaymentInfo" })
public class AddOfficePaymentInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddOfficePaymentInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendError(500);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// challan 5
		HttpSession session = request.getSession();
		int transaction_id = Integer.parseInt(request.getParameter("transaction_id"));
		
		
		
			Transaction transaction = postgreSQLDatabase.feePayment.Query.getFeeTransactionById(transaction_id);
			long reg_id = transaction.getReg_id();
			int sem=transaction.getSemester();
		System.out.println(request.getParameter("transaction_id"));
		if (request.getParameter("payment_method").toString().equals("demand_draft")) {
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("dd_no", request.getParameter("dd_no"));
			Long ref_id = 0L;
		
				
				ref_id = Query.addFeePayment("DD payment", 5, details, -Long.parseLong(request.getParameter("amount")),
						reg_id,reg_id,sem);
				postgreSQLDatabase.registration.Query.updateVerificationStatus(3, reg_id);
		
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());

		}

		if (request.getParameter("payment_method").equals("neft")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", request.getParameter("amount"));
			details.put("bank", request.getParameter("bank"));
			details.put("ifsc", request.getParameter("�fsc"));
			details.put("date", request.getParameter("date"));
			details.put("tid", request.getParameter("tid"));
			Long ref_id = 0L;
			
				ref_id = Query.addFeePayment("NEFT payment", 4, details, -Long.parseLong(request.getParameter("amount")),
						reg_id,	reg_id,sem);
			
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
		if (request.getParameter("payment_method").equals("challan")) {
		
			JSONObject details = new JSONObject();
			details.put("amount", -Long.parseLong(request.getParameter("amount")));
			details.put("bank", request.getParameter("bank"));
			details.put("date", request.getParameter("date"));
			details.put("challan_no", request.getParameter("challan_no"));
		Long ref_id = 0L;
			
				ref_id = Query.addFeePayment("Challan payment", 2, details,
						-Long.parseLong(request.getParameter("amount")),
						reg_id, reg_id,sem);
			
			PrintWriter writer = response.getWriter();
			JSONObject ref = new JSONObject();
			ref.put("ref_id", ref_id);
			writer.write(ref.toString());
		}
	}

}
