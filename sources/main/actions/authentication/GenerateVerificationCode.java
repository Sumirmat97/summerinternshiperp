package actions.authentication;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.mortbay.jetty.security.UserRealm;

/**
 * Servlet implementation class GenerateVerificationCode
 */
@WebServlet("/GenerateVerificationCode")
public class GenerateVerificationCode extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenerateVerificationCode() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.sendError(500);
		doPost(request, response);

			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username=request.getParameter("username");
		System.out.println(username);
		Long user_id=postgreSQLDatabase.authentication.Query.getUserId(username);
		String user_email=postgreSQLDatabase.authentication.Query.getUserEmail(username);
		PrintWriter writer=response.getWriter();
		JSONObject status=new JSONObject();
		
		if(user_email!=null){
			if(user_id!=0){
				status.put("success", true);
			String vercode=utilities.Random.generateRandomString(20);
			postgreSQLDatabase.authentication.Query.updateVercode(user_id, vercode);
			email.SendEmail.sendHTMLEmail("meghagupta.mg04@gmail.com","Reset Password","<html><body><a href=\"http://localhost:9999/erp/VerifyForgotPassword?vercode="+vercode+"&username="+username+"\">Click here to reset your password</a></body</html>");
				
		}
			else{
				status.put("success", false);
				status.put("message","Incorrect Email address");
				}
		}
		else{
			status.put("success", false);
			status.put("message","Incorrect Email address");
		}
		
		writer.write(status.toString());
		
	}

}
