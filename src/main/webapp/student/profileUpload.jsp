<html>
			<head>
			<link rel="stylesheet"
				href="../plugins/cropper/assets/css/font-awesome.min.css">
			<link rel="stylesheet"
				href="../plugins/cropper/assets/css/bootstrap.min.css">
				<script src="../plugins/cropper/dist/cropper.js"></script>
			<link rel="stylesheet" href="../plugins/cropper/dist/cropper.css">
			<link rel="stylesheet" href="../plugins/cropper/demo/css/main.css">
				<script src="../plugins/cropper/assets/js/jquery.min.js"></script>
				<script src="../plugins/cropper/assets/js/bootstrap.min.js"></script>
				<script src="../plugins/cropper/dist/cropper.js"></script>
				<script src="../plugins/cropper/demo/js/main.js"></script>
			
			<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
			<!--[if lt IE 9]>
			    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
			  <![endif]-->
			<style type="text/css">
				canvas{
					height:200px;
				}
			</style>  
			  
			</head>
			<body>
				<!--[if lt IE 8]>
			    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
			  <![endif]-->
			
				<!-- Content -->
				<div class="container">
					<div class="row">
						<div class="col-md-9">
							<div class="img-container" style="margin-top:2%;">
								<img id="image" src="" alt="Picture" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-9 docs-buttons">
							<!-- <h3 class="page-header">Toolbar:</h3> -->
							<div class="btn-group">
								<button type="button" class="btn btn-primary"
									data-method="setDragMode" data-option="move" title="Move">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrows"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary"
									data-method="setDragMode" data-option="crop" title="Crop">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-crop"></span>
									</span>
								</button>
							</div>
			
							<div class="btn-group">
								<button type="button" class="btn btn-primary" data-method="zoom"
									data-option="0.1" title="Zoom In">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-search-plus"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary" data-method="zoom"
									data-option="-0.1" title="Zoom Out">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-search-minus"></span>
									</span>
								</button>
							</div>
			
							<div class="btn-group">
								<button type="button" class="btn btn-primary" data-method="move"
									data-option="-10" data-second-option="0" title="Move Left">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrow-left"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary" data-method="move"
									data-option="10" data-second-option="0" title="Move Right">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrow-right"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary" data-method="move"
									data-option="0" data-second-option="-10" title="Move Up">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrow-up"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary" data-method="move"
									data-option="0" data-second-option="10" title="Move Down">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrow-down"></span>
									</span>
								</button>
							</div>
			
							<div class="btn-group">
								<button type="button" class="btn btn-primary" data-method="rotate"
									data-option="-45" title="Rotate Left">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-rotate-left"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary" data-method="rotate"
									data-option="45" title="Rotate Right">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-rotate-right"></span>
									</span>
								</button>
							</div>
			
							<div class="btn-group">
								<button type="button" class="btn btn-primary" data-method="scaleX"
									data-option="-1" title="Flip Horizontal">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrows-h"></span>
									</span>
								</button>
								<button type="button" class="btn btn-primary" data-method="scaleY"
									data-option="-1" title="Flip Vertical">
									<span class="docs-tooltip" data-toggle="tooltip">
										<span class="fa fa-arrows-v"></span>
									</span>
								</button>
							</div>
				
			
							<div class="btn-group">
								<label class="btn btn-primary btn-upload" for="inputImage"
									title="Upload image file"> <input type="file"
									class="sr-only" id="inputImage" name="file" accept="image/*">
									<span class="docs-tooltip" data-toggle="tooltip"
									title="Import image with Blob URLs"> Browse</span>
								</span>
								</label>
							</div>
			
							<div class="btn-group btn-group-crop">
								<button type="button" class="btn btn-primary"
									data-method="getCroppedCanvas">
									<span class="docs-tooltip" data-toggle="tooltip"><span
										class="fa fa-upload"></span>
								</button>
							</div>
			
							<!-- Show the cropped image in modal -->
							<div class="modal fade docs-cropped" id="getCroppedCanvasModal"
								aria-hidden="true" aria-labelledby="getCroppedCanvasTitle"
								role="dialog" tabindex="-1">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="getCroppedCanvasTitle">Preview</h4>
										</div>
										<div class="modal-body"></div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal">Cancel</button>
											<a class="btn btn-primary" id="download" onclick="uploadFile()"
												href="">Upload</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			
				<!-- Scripts -->
		
				
			</body>
					</html>