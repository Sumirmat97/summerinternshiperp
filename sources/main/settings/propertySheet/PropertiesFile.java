/**
 * 
 */
package settings.propertySheet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @author Joey
 *
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.SimplePBEConfig;
import org.jasypt.properties.EncryptableProperties;
import org.jasypt.properties.PropertyValueEncryptionUtils;

public class PropertiesFile {
	public static String servletPath;

	public static String getProperty(String property) throws IOException{
		SimplePBEConfig config = new SimplePBEConfig(); 
		config.setPassword("iiitkota");

		StandardPBEStringEncryptor encryptor = new org.jasypt.encryption.pbe.StandardPBEStringEncryptor();
		encryptor.setConfig(config);
		encryptor.initialize();
		Properties prop=new Properties();
		InputStream input=null;
		try{
			input = new FileInputStream(servletPath+File.separator+"config.properties");
		}
		catch(FileNotFoundException e){
			input = new FileInputStream("config.properties");

		}
		// load a properties file
		prop.load(input);
		String cipher=prop.getProperty(property);
		return PropertyValueEncryptionUtils.decrypt(cipher, encryptor);



	}
	public static String getDBServer() throws IOException{
		return getProperty("dbserver");
	}
	public static String getDeploymentURL() throws IOException{
		return getProperty("deployURL");
	}

	public static String getDBUser() throws IOException{
		return getProperty("dbuser");
	}

	public static String getDBPassword() throws IOException{
		return getProperty("dbpassword");
	}

	public static String getLDAPServer() throws IOException{
		return getProperty("ldapserver");
	}
	public static String getLDAPPassword() throws IOException{
		return getProperty("ldappassword");
	}
	public static String getLDAPPort() throws IOException{
		return getProperty("ldapport");
	}
	public static String getDBPort() throws IOException{
		return getProperty("dbport");
	}
	public static String getDBName() throws IOException{
		return getProperty("dbname");
	}
	public static String getFTPServer() throws IOException{
		return getProperty("ftpserver");
	}
	public static String getFTPUser() throws IOException{
		return getProperty("ftpuser");
	}
	public static String getFTPPassword() throws IOException{
		return getProperty("ftppassword");
	}
	public static String getNeo4jServer() throws IOException{
		return getProperty("neo4jserver");
	}
	public static String getNeo4jPort() throws IOException{
		return getProperty("neo4jport");
	}


	public static void main(String[] args) {
		try {
			generatePropertiesFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public static void generatePropertiesFile(){
		SimplePBEConfig config = new SimplePBEConfig(); 
		config.setPassword("iiitkota");

		StandardPBEStringEncryptor encryptor = new org.jasypt.encryption.pbe.StandardPBEStringEncryptor();
		encryptor.setConfig(config);
		encryptor.initialize();




		try {

			OutputStream output = new FileOutputStream("config.properties");
			Properties prop=new Properties();
			prop.setProperty("dbname", PropertyValueEncryptionUtils.encrypt("iiitk_erp", encryptor));
			prop.setProperty("dbserver", PropertyValueEncryptionUtils.encrypt("172.16.40.26", encryptor));
			prop.setProperty("ftpserver", PropertyValueEncryptionUtils.encrypt("172.16.40.26", encryptor));
			prop.setProperty("ftpuser", PropertyValueEncryptionUtils.encrypt("ftp", encryptor));
			prop.setProperty("ftppassword", PropertyValueEncryptionUtils.encrypt("ftp@1234", encryptor));
			prop.setProperty("dbuser", PropertyValueEncryptionUtils.encrypt("developer", encryptor));
			prop.setProperty("dbpassword", PropertyValueEncryptionUtils.encrypt("erp@master2013", encryptor));
			prop.setProperty("dbport", PropertyValueEncryptionUtils.encrypt("5432", encryptor));
			prop.setProperty("ldapserver", PropertyValueEncryptionUtils.encrypt("172.16.40.22", encryptor));
			prop.setProperty("ldappassword", PropertyValueEncryptionUtils.encrypt("iiitk@kota2013", encryptor));
			prop.setProperty("ldapport", PropertyValueEncryptionUtils.encrypt("389", encryptor));
			prop.setProperty("deployURL", PropertyValueEncryptionUtils.encrypt("http://172.16.40.26:8080", encryptor));
			prop.setProperty("neo4jserver", PropertyValueEncryptionUtils.encrypt("172.16.40.26", encryptor));
			prop.setProperty("neo4jport", PropertyValueEncryptionUtils.encrypt("7474", encryptor));
			prop.store(output, "Configuration File");
			output.close();
		} catch (IOException io) {
			io.printStackTrace();

		}
	}
}
