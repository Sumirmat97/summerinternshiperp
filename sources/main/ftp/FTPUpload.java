package ftp;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
/**
 * 
 * 
 * @author giri
 */
public class FTPUpload {
	static FTPClient ftpClient;
	public static void main(String[] args) throws IOException {

		//	createDirectory("shivani/test");
		//	uploadFile("/home/giri/ftp1.txt","home/test.txt");
		//System.out.println(listDirectories("shivani"));
		System.out.println(readTextFile("template/demo.txt"));


	}



	public static FTPClient getConnection()  {
		try{

			//if(ftpClient==null || !ftpClient.isConnected()||ftpClient.getStatus().equals("421"))
			{
				String server = settings.propertySheet.PropertiesFile.getFTPServer();
				int port = 21;
				
				ftpClient = new FTPClient();
				ftpClient.connect(server, port);
				
				ftpClient.login(settings.propertySheet.PropertiesFile.getFTPUser(),settings.propertySheet.PropertiesFile.getFTPPassword());
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			}
		}
		catch(Exception e){
e.printStackTrace();
		}

		return ftpClient;




	}

	public static void downloadFile(String remoteFilePath , String localFilePath) {
		try (FileOutputStream fos = new FileOutputStream(localFilePath)) {
			getConnection().retrieveFile(remoteFilePath, fos);
			getConnection().disconnect();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String readTextFile(String remoteFilePath ) {
		try {
			
			InputStream stream = getConnection().retrieveFileStream(remoteFilePath);
			
			String text=utilities.ServletUtils.getStringFromInputStream(stream);
			stream.close();
			getConnection().disconnect();
			return text;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	public static InputStream downloadFile(String remoteFilePath) {
		InputStream stream = null;
			try {
				 stream = getConnection().retrieveFileStream(remoteFilePath);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return stream;
			
	}


	public static boolean uploadFile(String localFilePath,String remoteFilePath) {
		File firstLocalFile = new File(localFilePath);
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(firstLocalFile);
			getConnection().disconnect();			
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean done = false;
		try {
			done = getConnection().storeFile(remoteFilePath, inputStream);
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			inputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return done;
	}
	
	public static boolean uploadFile(InputStream inputStream,String remoteFilepath){
		boolean done=false;
		try {
			done = getConnection().storeFile(remoteFilepath, inputStream);
			inputStream.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			getConnection().disconnect();
			inputStream.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return done;
	}
		
		
		
		
	
	public static boolean createDirectory(String pathname){
		try {

			getConnection().mkd(pathname);
			getConnection().disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;


	}
	public static boolean deleteFile(String file){
		boolean success=false;
		try {
			getConnection().deleteFile(file);
			success=true;
			getConnection().disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}
	public static boolean deleteDirectory(String pathname){
		try {
			getConnection().mlistDir(pathname);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		}
		try {
			getConnection().rmd(pathname);
			getConnection().disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;



	}

	public static ArrayList<String> listDirectories(String parent){
		ArrayList<String> folders=new ArrayList<String>();
		try {
			FTPFile[] list = getConnection().listDirectories(parent);
			for(FTPFile folder:list){
				folders.add(folder.getName());
			}
			getConnection().disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return folders;

	}

	public static ArrayList<String> listFile(String pathname){
		ArrayList<String> files=new ArrayList<String>();
		try {
			FTPFile[] filelist = getConnection().listFiles(pathname);
			for(FTPFile file:filelist){
				files.add(file.getName());
			}
			getConnection().disconnect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return files;
	}
  

}