package actions.templates;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import postgreSQLDatabase.documents.Documents;
import postgreSQLDatabase.file.File;
import postgreSQLDatabase.templates.Template;

/**
 * Servlet implementation class DeleteTemplate
 */
@WebServlet("/DeleteTemplate")
public class DeleteTemplate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteTemplate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("reached!!");
		PrintWriter writer = response.getWriter();
		Long template_id=Long.parseLong(request.getParameter("template_id"));
		try {
			 Template temp = postgreSQLDatabase.templates.Query.getTemplates(template_id);
			long file_id=temp.getFile();
			 File file=temp.getFile_wrapper();
			String directory = file.getDirectory();
			String file_name = file.getFile_name();
			String extension=file.getExtension();
			String file_path = directory+"/"+file_name;
			System.out.println(file_path);
			boolean success = ftp.FTPUpload.deleteFile(file_path);
	postgreSQLDatabase.templates.Query.deleteTemplate(template_id);
	

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
