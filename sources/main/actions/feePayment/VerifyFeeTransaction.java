package actions.feePayment;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import exceptions.IncorrectFormatException;
import postgreSQLDatabase.feePayment.Transaction;


/**
 * Servlet implementation class VerifyFeeTransaction
 */
@WebServlet("/VerifyFeeTransaction")
public class VerifyFeeTransaction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerifyFeeTransaction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
long transaction_id=Long.parseLong(request.getParameter("transaction_id"));
		//System.out.println("hello");
		
		
		
		boolean completed = postgreSQLDatabase.feePayment.Query.isVerifyFeePayments(transaction_id);
		if(completed==true){
			postgreSQLDatabase.feePayment.Query.verifyFeeTransaction(transaction_id);
			Transaction transaction=	postgreSQLDatabase.feePayment.Query.getFeeTransactionById(transaction_id);
			try {
				System.out.println(transaction_id);
				System.out.println(transaction.isCompleted());
				System.out.println(postgreSQLDatabase.registration.Query.getRegistrationStudentData(transaction.getReg_id()));
				if(transaction.isCompleted()&&postgreSQLDatabase.registration.Query.getRegistrationStudentData(transaction.getReg_id()).getSemester()==1)postgreSQLDatabase.registration.Query.updateVerificationStatus(4, transaction.getReg_id());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IncorrectFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		//System.out.println(completed);
	
		
		JSONObject message=new JSONObject();
		message.put("transaction_complete",completed);
	response.getWriter().write(message.toString());	
		
		
		
		
	}

}
