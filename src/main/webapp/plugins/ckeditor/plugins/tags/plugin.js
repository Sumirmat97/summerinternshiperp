CKEDITOR.plugins.add( 'tags',
{   
   requires : ['richcombo'], //, 'styles' ],
   init : function( editor )
   {
      var config = editor.config,
         lang = editor.lang.format;

      // Gets the list of tags from the settings.
      var tags=[];
    		var xmlhttp;
    		try{
    			xmlhttp = new XMLHttpRequest();
    		} catch (e){
    			// Internet Explorer Browsers
    			try{
    				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    			} catch (e) {
    				try{
    					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    				} catch (e){
    				//Browser doesn't support ajax	
    					alert("Your browser is unsupported");
    				}
    			}
    		}	
    		
    		if(xmlhttp){
    		    xmlhttp.onreadystatechange=function() {
    		    	
    		        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
    		        	
    		        	var json=JSON.parse(xmlhttp.responseText);
    		        	var data=JSON.stringify(json);
    		        	for(var i=0;i<json.length;i++){
    		        		var tag=[]
    		        		tag.push("{("+json[i].tagname+")}");
    		        		if(json[i].users=='General'){
    		        			tag.push(json[i].attributes);
    		        			tag.push(json[i].users);
    		        		}
    		        		else{
    		        			tag.push(json[i].users+" "+json[i].attributes);
        		        		tag.push(json[i].users+" "+json[i].attributes);	
    		        		}
    		        		 
    		        		tags.push(tag);
    		        	}
    		        	JSON.stringify(tags);
    				}
    		        if(xmlhttp.status == 404)
    					alert("Could not connect to server");
    				}
    		    xmlhttp.open("POST","../GetAllTags",true);
    			xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    		    xmlhttp.send();
    		}

    		
    	


     
      // Create style objects for all defined styles.
      editor.ui.addRichCombo( 'tags',
         {
            label : "Insert Tags",
            title :"Insert Tags",
            voiceLabel : "Insert Tag",
            className : 'cke_format',
            multiSelect : false,

            panel :
            {
            	css : [ config.contentsCss, CKEDITOR.getUrl('skins/moono/editor.css' ) ],
               voiceLabel : lang.panelVoiceLabel
            },

            init : function()
            {
               this.startGroup( "" );
               //this.add('value', 'drop_text', 'drop_label');
               for (var this_tag in tags){
                  this.add(tags[this_tag][0], tags[this_tag][1], tags[this_tag][2]);
               }
            },

            onClick : function( value )
            {         
               editor.focus();
               editor.fire( 'saveSnapshot' );
               editor.insertHtml(value);
               editor.fire( 'saveSnapshot' );
            }
         });
   }
});